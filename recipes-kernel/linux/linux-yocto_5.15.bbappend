KBRANCH:beaglebone-yocto-custom = "v5.15/standard/beaglebone"

KMACHINE:beaglebone-yocto-custom ?= "beaglebone"

SRCREV_machine:beaglebone-yocto-custom ?= "9aabbaa89fcb21af7028e814c1f5b61171314d5a"

COMPATIBLE_MACHINE:beaglebone-yocto-custom = "beaglebone-yocto-custom"

LINUX_VERSION:beaglebone-yocto-custom = "5.15.54"
